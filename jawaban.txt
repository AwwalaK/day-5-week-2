Nama : Awwala Nisa Kamila
Tugas : Day - 5, Week -2 (Berlatih SQL)

1. Membuat Database
Jawab : create database myshop;

2. Membuat table di dalam database
Jawab : 
use myshop;
create table users (
	id int auto_increment,
	name varchar(255),
	email varchar(255),
	password varchar(255),
	primary key (id)
);

create table categories (
	id int auto_increment,
	name varchar(255),
	primary key(id)
);

create table items (
	id int auto_increment,
	name varchar(255),
	description varchar(255),
	price int,
	stock int,
	category_id int,
	primary key (id),
	foreign key (id) references categories(id)
);

show tables;

3. Memasukkan data pada table
Jawab : 
insert into users (name, email, password)
values 
	('John Doe', 'john@doe.com', 'john123'), 
	('Jane Doe', 'jane@doe.com', 'jenita123');

insert into categories (name)
values 
	('gadget'),
	('cloth'),
	('men'),
	('women'),
	('branded');

insert into items (name, description, price, stock, category_id)
values
	('Sumsang b50', 'hape keren dari merek sumsang', 4000000, 100, 1),
	('Uniklooh', 'baju keren dari brand ternama', 500000, 50, 2),
	('IMHO Watch', 'jam tangan anak yang jujur banget', 2000000, 10, 1);

4. Mengambil data dari database
a. Mengambil data users
Jawab : select name, email from users;

b. Mengambil data items
Jawab :
- select * from items where price > 1000000;
- select * from items where name like '%watch';

c. Menampilkan data items join dengan kategori
Jawab : 
select items.name, items.description, items.price, items.stock, items.category_id, categories.name as 'kategori'
from categories
inner join items on categories.id = items.id;

5. Mengubah data dari database
Jawab : 
update items
set price = 2500000
where name = 'Sumsang b50';








